#include "ft_ping.h"

/*
 * Handle all getaddrinfo() error code, you can find them in: man 3 getaddrinfo
 */
void		ft_getaddrinfo_error(int error_code, char *arg)
{
	switch (error_code)
	{
		case EAI_NONAME:
			ft_putstr(ft_strjoin("./ft_ping: unknow host: ", arg));		
			ft_putchar('\n');
			break;	
		case EAI_SERVICE:
			ft_putstr("requested service is not available for the requested socket type\n");		
			break;
		case EAI_SOCKTYPE:
			ft_putstr("The requested socket type is not supported\n");		
			break;
	}
}

void		ft_recvmsg_ret(int msg_flag)
{
	ft_putstr("recvmsg: ");
	switch (msg_flag)
	{
		case MSG_EOR:
			ft_putendl("end-of-record");
		case MSG_TRUNC:
			ft_putendl("data larger than the buffer supplied");
		case MSG_CTRUNC:
			ft_putendl("lack of space in buffer");
		case MSG_OOB:
			ft_putendl("out-of-band data were received");
		case MSG_ERRQUEUE:
			ft_putendl("no data was received");
	}
}
/*
void		ft_recvmsg_error(int msg_ret)
{
	switch (msg_ret)
	{
		case EAGAIN:
			ft_putendl("nonblocking socket and the receive operation would  block");
		case EWOULDBLOCK:
			ft_putendl("nonblocking socket and the receive operation would  block");
		case EBADF:
			ft_putendl("The argument sockfd is an invalid descriptor");
		case ECONNREFUSED:
			ft_putendl("network connection was refused by host");
		case EFAULT:
			ft_putendl("receive buffer pointer(s) point outside the process's address space");
		case EINTR:
			ft_putendl("receive was interrupted by delivery of a signal before any data were available");
		case EINVAL:
			ft_putendl("Invalid argument passed");
		case ENOMEN:
			ft_putendl("Could not allocate memory for recvmsg()");
		case ENOTCONN:
			ft_putendl("socket is associated with a connection-oriented protocol and has not been connected");
		case ENOTSOCK:
			ft_putendl("The argument sockfd does not refer to a socket");
	}
}
*/
