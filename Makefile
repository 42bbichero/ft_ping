NAME = ft_ping

# LIBFT
LFTPATH = ../libft
LFTFILE = $(LFTPATH)/libft.a
NETLFTPATH = ../netlibft
NETLFTFILE = $(NETLFTPATH)/netlibft.a

OBJPATH = obj
SRCPATH = .

CC = gcc
BASEFLAGS = -Wall -Wextra
CFLAGS = $(BASEFLAGS) -Werror -O2 -g

SRCSFILES = ft_main.c ft_time.c ft_return.c ft_packet.c

SRC = $(addprefix $(SRCPATH)/,$(SRCSFILES))
OBJECTS = $(SRC:$(SRCPATH)/%.c=$(OBJPATH)/%.o)

RM = rm -rf

# Yellow
Y = \033[0;33m
# Red
R = \033[0;31m
# Green
G = \033[0;32m
# No color
E = \033[39m

all: libft netlibft $(NAME)

$(NAME): $(OBJECTS)
	@echo "$(Y)[FT_PING]$(E)"
	@echo "$(G)[COMPILING]$(E)"
	@echo "$(G)$(CC) -o $@ $(CFLAGS) $(OBJECTS) $(LFTFILE) $(NETLFTFILE) $(E)"
	@$(CC) -o $@ $(CFLAGS) $(OBJECTS) $(LFTFILE) $(NETLFTFILE)
	@echo "$(G)[COMPILING DONE]$(E)"

$(OBJECTS): $(OBJPATH)/%.o : $(SRCPATH)/%.c
	@echo "$(Y)[CREATE OBJ DIR]$(E)"
	@mkdir -p $(dir $@)
	@$(CC) -o $@ $(CFLAGS) -c $<

clean:
	@echo "$(Y)[FT_PING]$(E)"
	@echo "$(R)[REMOVE OBJ DIR]$(E)"
	@$(RM) $(OBJPATH)
	@cd $(LFTPATH) && $(MAKE) -s clean
	@cd $(NETLFTPATH) && $(MAKE) -s clean

fclean: clean
	@$(RM) $(NAME)
	@cd $(LFTPATH) && $(MAKE) -s fclean
	@cd $(NETLFTPATH) && $(MAKE) -s fclean

libft:
	@cd $(LFTPATH) && $(MAKE) -s
netlibft:
	@cd $(NETLFTPATH) && $(MAKE) -s

re: fclean all
