#ifndef FT_PING_H
# define FT_PING_H
# include <stdio.h>
# include <sys/types.h>
# include <sys/time.h>

#include <netinet/in.h>
#include <net/if.h>
#include <sys/ioctl.h>

# include "../libft/libft.h"
# include "../netlibft/netlibft.h"

# define PACKET_SIZE 48
# define MSG_BUFF_SIZE 1024

/*
 * ft_getopt() use
 */
typedef struct	s_globalArgs
{
	int 	count;		/* -c option */
	int 	verbosity;	/* -v option */
	//	int 	help;		/* -h option */
	char 	*hostname;	/* require argument hostname  */
}		t_globalArgs;

typedef struct	s_packetTime
{
	int	sec;
	int	usec;
}		t_packetTime;

/* total ip header length: 20 bytes (=160 bits) */
typedef struct 	s_ipheader
{
	unsigned char ip_hl:4, ip_v:4; /* this means that each member is 4 bits */
	unsigned char ip_tos;
	unsigned short int ip_len;
	unsigned short int ip_id;
	unsigned short int ip_off;
	unsigned char ip_ttl;
	unsigned char ip_p;
	unsigned short int ip_sum;
	unsigned int ip_src;
	unsigned int ip_dst;
}		t_ipheader;

/*
   The Internet Protocol is the network layer protocol, used for routing the
   data from the source to its destination. Every datagram contains an IP header
   followed by a transport layer protocol such as tcp.

# ip_hl: the ip header length in 32bit octets. this means a value of 5
for the hl means 20 bytes (5 * 4). values other than 5 only need to
be set it the ip header contains options (mostly used for routing)
# ip_v: the ip version is always 4 (maybe I'll write a IPv6 tutorial later;)
# ip_tos: type of service controls the priority of the packet. 0x00 is
normal. the first 3 bits stand for routing priority, the next 4 bits
for the type of service (delay, throughput, reliability and cost).
# ip_len: total length must contain the total length of the ip datagram.
this includes ip header, icmp or tcp or udp header and payload size in bytes.
# ip_id: the id sequence number is mainly used for reassembly of fragmented IP
datagrams. when sending single datagrams, each can have an arbitrary ID.
# ip_off: the fragment offset is used for reassembly of fragmented datagrams.
the first 3 bits are the fragment flags, the first one always 0, the second
the do-not-fragment bit (set by ip_off |= 0x4000) and the third the more-flag
or more-fragments-following bit (ip_off |= 0x2000). the following 13 bits is
the fragment offset, containing the number of 8-byte big packets already sent.
# ip_ttl: time to live is the amount of hops (routers to pass) before the packet
is discarded, and an icmp error message is returned. the maximum is 255.
# ip_p: the transport layer protocol. can be tcp (6), udp(17), icmp(1), or
whatever protocol follows the ip header. look in /etc/protocols for more.
# ip_sum: the datagram checksum for the whole ip datagram. every time anything
in the datagram changes, it needs to be recalculated, or the packet will
be discarded by the next router. see V. for a checksum function.
ip_src and ip_dst: source and destination IP address, converted to long
format, e.g. by inet_addr(). both can be chosen arbitrarily.
 */

/* total icmp header length: 8 bytes (=64 bits) */
/* The following data structures are ICMP type specific */
typedef struct	s_icmpheader
{
	unsigned char c_icmp_type;
	unsigned char c_icmp_code;
	unsigned short int c_icmp_cksum;
	unsigned short int c_icmp_seq;
	unsigned short int c_icmp_id;
}		t_icmpheader;

/*
# icmp_type: the message type, for example 0 - echo reply, 8 - echo request, 3 -
destination unreachable. look in <netinet/ip_icmp.h> for all the types.
# icmp_code: this is significant when sending an error message (unreach), and
specifies the kind of error. again, consult the include file for more.
# icmp_cksum: the checksum for the icmp header + data. same as the IP checksum.
# Note: The next 32 bits in an icmp packet can be used in many different ways.
This depends on the icmp type and code. the most commonly seen structure, an
ID and sequence number, is used in echo requests and replies, hence we only
use this one, but keep in mind that the header is actually more complex.
# icmp_id: used in echo request/reply messages, to identify the request
# icmp_seq: identifies the sequence of echo messages, if more than one is sent.
*/

/* All functions of ft_ping */
void		display_usage(void);
t_globalArgs	ft_initialise_args(t_globalArgs optArgs);
int		ft_process_args(t_globalArgs optArgs);
t_packetTime	*calculDiff(t_packetTime *snd, t_packetTime *rcv);	
void		ft_connect(struct addrinfo *result);
void		ft_getaddr(t_globalArgs optarg);
char		*ft_host_to_ip(struct addrinfo *sa);
void		ft_getaddrinfo_error(int error_code, char *arg);
void		ft_ping(int sfd, char *buff, struct addrinfo *result);
unsigned long 	ft_csum(unsigned short *buf, int nwords);
unsigned int 	ip_to_int(const char *ip);
void		ft_recvmsg_ret(int msg_flag);
void		ft_recvmsg_error(int msg_ret);
void		ft_fill_ipheader(t_ipheader *iph, struct addrinfo *dest, char *packet);
void		ft_fill_icmpheader(t_icmpheader *iph);
void		ft_fill_msgstruct(struct msghdr *msg, char *buffer, t_icmpheader *icmph, struct iovec *iov);

#endif
