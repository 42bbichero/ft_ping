#include "ft_ping.h"

void		ft_connect(struct addrinfo *result)
{
	//struct sockaddr_storage for handle ipv4 and ipv6
	/* this buffer will contain ip header, tcp header,
	and payload. we'll point an ip header structure
	at its beginning, and a tcp header structure athat to write the header values into it 
	that to write the header values into it */
	char packet[PACKET_SIZE] = "";
	int sfd;
	char *ip_addr;

	sfd = socket(result->ai_family, SOCK_RAW, IPPROTO_ICMP);
	//sfd = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
	if (sfd < 0)
	{
		ft_putendl("socket error");
		exit (-1);
	}

	// Get back ip address in char * printable format
	ip_addr = ft_host_to_ip(result);
	printf("ip: %s\n", ip_addr);

  	if (ip_addr == NULL)
	{
		ft_putendl("ai_family error");
		exit (-1);
	}
	ft_ping(sfd, packet, result);

	close(sfd);
}

void		ft_getaddr(t_globalArgs optArgs)
{
	struct addrinfo hints;
	struct addrinfo *result;
	int addrerror;

	// Make sure the structure is empty 
	ft_memset(&hints, 0, sizeof(hints));
	//hints.ai_flags = 0;
	hints.ai_family = AF_UNSPEC;    /* Allow IPv4 AND IPv6*/
	//hints.ai_socktype = SOCK_DGRAM; /* Datagram socket */
	hints.ai_socktype = SOCK_STREAM; /* TCP socket */
	//hints.ai_protocol = IPPROTO_ICMP; /* ICMP protocol */
	hints.ai_protocol = 0; /* Any protocols */

	/* getaddrinfo() returns a list of address structures.
	   Try each address until we successfully connect(2).
	   If socket(2) (or connect(2)) fails, we (close the socket
	   and) try the next address. */
	addrerror = getaddrinfo(optArgs.hostname, "echo", &hints, &result);
	if (addrerror != 0)
	{
		ft_getaddrinfo_error(addrerror, optArgs.hostname);
		//fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(addrerror));
		exit(-1);
	}

	ft_connect(result);
}

/*
 * Check arguments validity
 */
int		ft_process_args(t_globalArgs optArgs)
{
	printf("count: %d\n", optArgs.count);
	printf("verbose: %d\n", optArgs.verbosity);
	printf("hostname: %s\n", optArgs.hostname);

	if (optArgs.verbosity > 3 && optArgs.verbosity < 0)
	{
		ft_putstr("ft_ping: bad number for verbosity\n");
		return (-1);
	}
	if (optArgs.count != 0 && optArgs.count < 1)
	{
		ft_putstr("ft_ping: bad number of packets to transmit.\n");
		return (-1);
	}

	return (1);	
}

/*
 * Initialise all arguments
 */
t_globalArgs	ft_initialise_args(t_globalArgs optArgs)
{
	optArgs.count = 0;
	optArgs.verbosity = 0;
	optArgs.hostname = NULL;

	return (optArgs);
}

/*
 * main() function, with 2 basic arguments, int ac and char ** av
 */
int		main(int ac, char **av)
{
	t_globalArgs optArgs;
	int opt = 0;

	/* Define arguments, required or not, with value or not */
	char *optString = "c:v::h";

	/* If uid != 0 (root), abort */
	if (getuid() != 0)
	{
		ft_putstr("ft_ping must be run as root\n");
		return (-1);
	}

	/* If no argument print usage */
	if (ac < 2)
	{
		display_usage();
		return (0);
	}

	/* Initialize globalArgs before continu */
	optArgs = ft_initialise_args(optArgs);

	/* Getopt loop */
	while (optind < ac)
	{
		//printf("optind: %d\n", optind);
		if ((opt = ft_getopt(ac, av, optString)) != -1)
		{
			//printf("opt: %d\n", opt);
			switch (opt)
			{
				case 'c':
					//printf("optarg: %s\n", optarg);
					if (!ft_isnumber(optarg))
					{
						ft_putstr("ft_ping: bad number of packets to transmit.\n");
						return (-1);
					}
					optArgs.count = ft_atoi(optarg);
					break;

				case 'v':
					//printf("optarg: %s\n", optarg);
					optArgs.verbosity = ft_atoi(optarg);
					break;

				case 'h':
					display_usage();
					return (0);

				default:
					/* You won't actually get here. */
					break;
			}
		}
		else
		{
			//printf("arg: %s\n", av[optind]);
			optArgs.hostname = av[optind];
			optind++;
		}
	}
	if (ft_process_args(optArgs) > 0)
		ft_getaddr(optArgs);

	return (0);
}
