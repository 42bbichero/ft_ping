#include "ft_ping.h"

/*
 * Return time difference between 2 time (sec and micro sec)
 */
t_packetTime		*calcul_diff(t_packetTime *snd, t_packetTime *rcv)
{
	t_packetTime	*latency;	

	latency = (t_packetTime *)malloc(sizeof(t_packetTime));
	latency->sec = snd->sec - rcv->sec;
	latency->usec = snd->usec - rcv->usec;

	if (latency->sec < 0 || (latency->sec == 0 && latency->usec < 0))
	{
		ft_putstr("Packet time receive can't be higher than packet time send\n");
		exit (-1);
	}
	return (latency);
}

void	display_usage(void)
{
	ft_putstr("./ft_ping [-v verbose-level] [-c count] [-i interval] ... destination\nTest the reachability of a host.\n");
	/* ... */
	exit (EXIT_FAILURE);
}

/*
 * Convert a struct sockaddr address to a string, IPv4 and IPv6:
 */
char		*ft_host_to_ip(struct addrinfo *sa)
{
	char *str;

	str = NULL;
	switch (sa->ai_family)
	{
		case AF_INET:
			str = (char *)malloc(sizeof(char) * (INET_ADDRSTRLEN + 1));
			if (inet_ntop(AF_INET, &((struct sockaddr_in *)sa->ai_addr)->sin_addr, str, INET_ADDRSTRLEN) == NULL)
			{
				ft_putstr("inet_ntop() error\n");
				return (NULL);
			}
			break;

		case AF_INET6:
			str = (char *)malloc(sizeof(char) * (INET6_ADDRSTRLEN + 1));
			if (inet_ntop(AF_INET6, &((struct sockaddr_in6 *)sa->ai_addr)->sin6_addr, str, INET6_ADDRSTRLEN) == NULL)
			{
				ft_putstr("inet_ntop() error\n");
				return (NULL);
			}
			break;

		default:
			return (NULL);
	}
	return (str);
}

/*
 * Generate header checksums
 */
unsigned long	ft_csum(unsigned short *buf, int nwords)
{
	unsigned long sum;

	sum = 0;
	while (nwords > 0)
	{
		sum += *buf++;
		nwords--;
	}
	sum = (sum >> 16) + (sum & 0xffff);
	sum += (sum >> 16);

	// flip bit
	return (~sum);
}

/*
 * Create packet, send it and receive reply
 */
void		ft_ping(int sfd, char *packet, struct addrinfo *result)
{
	t_ipheader *iph = (t_ipheader *)packet;
	t_icmpheader *icmph = (t_icmpheader *)packet + sizeof(t_ipheader);
	struct sockaddr_in dest;
	struct msghdr msg;
	struct iovec iov; /* Data array */
	//	struct sock_extended_err *sock_err; /* Struct describing the error */ 
	//	struct sockaddr_in6 dest6;
	char *buffer;
	int numbytes;
	int one = 1;
	const int *val = &one;
	int msg_ret = 0, msg_flag = 0;

	//icmph = (t_icmpheader *)malloc(sizeof(t_icmpheader));
	//iph = (t_ipheader *)malloc(sizeof(t_ipheader));
	packet = (char *)malloc(sizeof(t_ipheader) + sizeof(t_icmpheader));
	buffer = (char *)malloc(sizeof(char) * MSG_BUFF_SIZE);
	ft_memset(packet, 0, PACKET_SIZE);	/* zero out the buffer */

	// Fill ip header
	ft_fill_ipheader(iph, result, packet);
	// Fill icmp header
	ft_fill_icmpheader(icmph);

	/* 
	 *  IP_HDRINCL must be set on the socket so that
	 *  the kernel does not attempt to automatically add
	 *  a default ip header to the packet
	 */
	if (setsockopt(sfd, IPPROTO_IP, IP_HDRINCL, val, sizeof(one)) < 0)
	{
		ft_putendl("setsockopt error");
		exit (-1);
	}

	// Following ip address type, send to ipv4 or ipv6 address
	/*
	   if (result->ai_family == AF_INET)
	   {
	   dest.sin_family = AF_INET;
	   dest.sin_addr = ((struct sockaddr_in *)result->ai_addr)->sin_addr;
	   numbytes = sendto(sfd, packet, PACKET_SIZE, MSG_CONFIRM, (struct sockaddr *)&dest, INET_ADDRSTRLEN);
	   }
	   else
	   numbytes = 0;
	//	else
	//	{
	//		dest.sin_family = AF_INET;
	//			numbytes = sendto(sfd, buff, BUFF_SIZE, MSG_CONFIRM, (struct sockaddr *)&dest6, INET6_ADDRSTRLEN);
	//	}
	 */
	/*
	 * Define recvmsg() parameter / options
	 */
	ft_fill_msgstruct(&msg, buffer, icmph, &iov);

	// Ip header len (20 bytes)
	iph->ip_len = sizeof(t_ipheader);
	printf("ip_len: %d\n", iph->ip_len);
	// First part is an IPv4 header.
	ft_memcpy(packet, iph, iph->ip_len);

	// Next part of packet is upper layer protocol header.
	ft_memcpy((packet + iph->ip_len), icmph, sizeof(t_icmpheader));

	// Finally, add the ICMP data.
	ft_memcpy((packet + iph->ip_len + sizeof(t_icmpheader)), packet, PACKET_SIZE);

	// Calcul checksum
	ft_memcpy((packet + iph->ip_len), icmph, sizeof(t_icmpheader));

	// The kernel is going to prepare layer 2 information (ethernet frame header) for us.
	// For that, we need to specify a destination for the kernel in order for it
	// to decide where to send the raw datagram. We fill in a struct in_addr with
	// the desired destination IP address, and pass this structure to the sendto() function.
	ft_memset(&dest, 0, sizeof (struct sockaddr_in));
	dest.sin_family = AF_INET;
	dest.sin_addr.s_addr = iph->ip_dst;

	while (1)
	{
		//printf("(uint32_t)ip dest: %d\n", (int)ft_sockaddr_to_ip(&dest));
		ft_memcpy(packet, &icmph, sizeof(icmph));
		ft_memcpy(packet + sizeof(icmph), "hello", 5); //icmp payload
		printf("packet: %s\n", packet);

		if ((numbytes = sendto(sfd, packet, iph->ip_len + sizeof(t_icmpheader) + PACKET_SIZE, 0, (struct sockaddr *)&dest, sizeof(struct sockaddr))) < 0)
			perror("sendto()");
		else
			printf("talker: sent %d bytes to %s\n", numbytes, ft_host_to_ip(result));

		// If recvmsg() function return negatif value, print it return error
		//ft_recvmsg_error(msg_ret);
		if ((msg_ret = recvmsg(sfd, &msg, MSG_ERRQUEUE)) < 0)
			perror("recvmsg()");
		else
		{
			ft_recvmsg_ret(msg_flag);
			printf("msg_name: %s\n", (char *)msg.msg_name);
		}
		printf("File descriptor: %d\n", sfd);
		sleep(1);
	}

	if (numbytes == -1)
	{
		ft_putendl("sendto error");
		exit(1);
	}
	freeaddrinfo(result);
}
