#include "ft_ping.h"

unsigned int ip_to_int(const char *ip)
{
	/* The return value. */
	unsigned v = 0;
	/* The count of the number of bytes processed. */
	int i;
	/* A pointer to the next digit to process. */
	const char * start;

	start = ip;
	for (i = 0; i < 4; i++)
	{
		/* The digit being processed. */
		char c;
		/* The value of this byte. */
		int n = 0;
		while (1) {
			c = * start;
			start++;
			if (c >= '0' && c <= '9')
			{
				n *= 10;
				n += c - '0';
			}
			/* We insist on stopping at "." if we are still parsing
			   the first, second, or third numbers. If we have reached
			   the end of the numbers, we will allow any character. */
			else if ((i < 3 && c == '.') || i == 3)
				break;
			else
				return (0);
		}
		if (n >= 256)
			return (0);
		v *= 256;
		v += n;
	}
	return (v);
}

void		ft_fill_ipheader(t_ipheader *iph, struct addrinfo *dest, char *packet)
{
	/*
	 * Only for IPv4
	 */
	// Source IPv4 address (32 bits)
	if (inet_pton(AF_INET, "127.0.0.1", &(iph->ip_src)) < 0)
	{
		ft_putendl("inet_pton() error with local ip");
		exit(-1);
	}
	// Destination IPv4 address (32 bits)
	if (inet_pton(AF_INET, ft_host_to_ip(dest), &(iph->ip_dst)) < 0)
	{
		ft_putendl("inet_pton() error with dest ip");
		exit(-1);
	}
	ft_putendl(ft_strjoin("dst_ip: ", ft_itoa(iph->ip_dst)));
	ft_putendl(ft_strjoin("src_ip: ", ft_itoa(iph->ip_src)));

	/* we'll now fill in the ip/tcp header values, see above for explanations */
	iph->ip_hl = 5;
	iph->ip_v = 4;
	iph->ip_tos = 0; /* type of service  */
	//iph->ip_len = sizeof(iph) + sizeof(icmph);	/* no payload */
	iph->ip_id = 1;	/* unique id for icmp packet */
	iph->ip_off = 0;
	iph->ip_ttl = 255; /* time to live */
	iph->ip_p = IPPROTO_ICMP; /* Protocol */
	iph->ip_sum = 0;
	iph->ip_sum = ft_csum((unsigned short *)packet, sizeof(packet));
}

void		ft_fill_icmpheader(t_icmpheader *icmph)
{
	/*
	 *  here the icmp packet is created
	 *  also the ip checksum is generated
	 */
	icmph->c_icmp_type = ICMP_ECHO;
	icmph->c_icmp_code = 0;
	icmph->c_icmp_id = 1;
	icmph->c_icmp_seq = 1;
	icmph->c_icmp_cksum = 0;
	//	ft_putendl(ft_strjoin("size icmpheader: ", ft_itoa((int)sizeof(t_icmpheader))));
	icmph->c_icmp_cksum = ft_csum((unsigned short *)icmph, sizeof(t_icmpheader));
	//iph->ip_sum = ft_csum((unsigned short *)iph, sizeof(struct iphdr));
}

void		ft_fill_msgstruct(struct msghdr *msg,char *buffer, t_icmpheader *icmph, struct iovec *iov)
{
	struct sockaddr remote;

	iov->iov_base = &icmph;
	iov->iov_len = sizeof(icmph);
	msg->msg_name = (void*)&remote;
	msg->msg_namelen = sizeof(remote);
	msg->msg_iov = iov;
	msg->msg_iovlen = 1;
	msg->msg_flags = 0;
	msg->msg_control = buffer;
	msg->msg_controllen = sizeof(buffer);
}
